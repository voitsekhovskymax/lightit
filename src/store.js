import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        auth: {},
        token: ''
    },
    getters: {
        getAuth(state) {
            return  state.auth;
        },
        getToken(state) {
            return  state.token;
        }
    },
    mutations: {
        set(state, {type, items}) {
            state[type] = items;
        },
    }
});

export default store;
