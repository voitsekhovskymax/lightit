import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import products from '@/components/products'
import register from '@/components/register'
import single_product from '@/components/single_product'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'products',
      component: products
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/single/:id',
      name: 'single_product',
      component: single_product
    }
  ]
})
